import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';

/**
 * Generated class for the ProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {
  restaurant: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cartProvider: CartProvider
  ) {
    this.restaurant = navParams.get("restaurant");
  }

  addItemToCart(item) {
    this.cartProvider.addItem(item);
    alert("Produto inserido no carrinho :)");
  }

}
