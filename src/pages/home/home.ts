import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestaurantsProvider } from '../../providers/restaurants/restaurants';
import { ProductsPage } from '../products/products';
import { CartPage } from '../cart/cart';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  restaurants: any = [];

  constructor(
    public navCtrl: NavController,
    private restaurantsProvider: RestaurantsProvider
  ) {
    this.restaurants = restaurantsProvider.getRestaurants();
  }

  goToProducts(restaurant) {
    this.navCtrl.push(ProductsPage, {
      restaurant: restaurant
    })
  }

  goToCart(){
    this.navCtrl.push(CartPage);
  }

}
