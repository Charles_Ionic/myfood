import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';

/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  items: any = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cartProvider: CartProvider
  ) {
    this.items = cartProvider.getItems();
  }

  getTotal(){
    var total = 0;
    this.cartProvider.getItems().forEach(product => {
      total += product.price;
    });

    return total;
  }

  pay(){
    alert("Pedido efetuado com sucesso.");
  }

}
