import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CartProvider {
  CART_STORAGE_NAME = "CART_STORAGE";
  constructor() {

  }

  addItem(item) {
    var items = [];
    var items_localstorage = localStorage.getItem(this.CART_STORAGE_NAME);
    if(items_localstorage){
      items = JSON.parse(items_localstorage);
      items = items.filter(it => it.id != item.id);
    }
    items.push(item);
    localStorage.setItem(this.CART_STORAGE_NAME, JSON.stringify(items));
  }

  getItems(){
    var items = [];
    var items_localstorage = localStorage.getItem(this.CART_STORAGE_NAME);
    if(items_localstorage){
      items = JSON.parse(items_localstorage);
    }
    return items;
  }

  removeItem(id) {
    var items = [];
    var items_localstorage = localStorage.getItem(this.CART_STORAGE_NAME);
    if(items_localstorage){
      items = JSON.parse(items_localstorage);
      items = items.filter(it => it.id != id);
    }
    localStorage.setItem(this.CART_STORAGE_NAME, JSON.stringify(items));
  }

  clearCart() {
    localStorage.removeItem(this.CART_STORAGE_NAME);
  }

}
