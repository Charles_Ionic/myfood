import { Injectable } from '@angular/core';

/*
  Generated class for the RestaurantsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestaurantsProvider {

  constructor() {

  }

  getRestaurants() {
    var restaurants = [
      {
        name: "MC Donalds",
        type: "Fast Food",
        image: "mcdonalds.png",
        products: [
          {
            id: 1,
            name: "Combo Oferta do dia 1",
            image: "lanche1_mac.png",
            price: 20.00
          },
          {
            id: 2,
            name: "Combo Oferta do dia 1",
            image: "lanche2_mac.png",
            price: 25.00
          }
        ]
      },
      {
        name: "Lig Lig",
        type: "Comida Japonesa",
        image: "liglig.png",
        products: [
          {
            id: 3,
            name: "Combo Oferta do dia 1",
            image: "lig-lig-1.png",
            price: 10.00
          },
          {
            id: 4,
            name: "Combo Oferta do dia 1",
            image: "lig-lig-2.png",
            price: 15.00
          }
        ]
      }
    ]
    return restaurants;
  }

}
